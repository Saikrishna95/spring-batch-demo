package com.zeta.springbatchdemo.mapper;

import com.zeta.springbatchdemo.model.ReverseHandOffEntry;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ReverseHandoffEntryRowMapper implements RowMapper<ReverseHandOffEntry> {

    @Override
    public ReverseHandOffEntry mapRow(ResultSet resultSet, int i) throws SQLException {
        return new ReverseHandOffEntry("PZAP", //TODO move to config
                resultSet.getString("unique_id"),
                resultSet.getString("received_ucic"),
                resultSet.getString("uploaded_ucic"),
                resultSet.getString("status"),
                resultSet.getString("reason_of_rejection"));
    }
}