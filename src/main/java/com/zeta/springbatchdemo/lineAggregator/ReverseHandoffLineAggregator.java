package com.zeta.springbatchdemo.lineAggregator;

import com.zeta.springbatchdemo.model.ReverseHandOffEntry;
import org.springframework.batch.item.file.transform.LineAggregator;

import java.util.StringJoiner;

public class ReverseHandoffLineAggregator implements LineAggregator<ReverseHandOffEntry> {
    @Override
    public String aggregate(ReverseHandOffEntry reverseHandOffEntry) {
        StringJoiner data = new StringJoiner("|");
        data.add(reverseHandOffEntry.getSourceSystem())
                .add(reverseHandOffEntry.getUniqueId())
                .add(reverseHandOffEntry.getReceivedUCIC())
                .add(reverseHandOffEntry.getUploadedUCIC())
                .add(reverseHandOffEntry.getStatus())
                .add(reverseHandOffEntry.getReasonOfRejection());
        return data.toString();
    }
}
