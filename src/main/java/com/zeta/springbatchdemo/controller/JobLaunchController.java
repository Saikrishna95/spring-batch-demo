package com.zeta.springbatchdemo.controller;

import com.zeta.springbatchdemo.service.JobLaunchService;
import com.zeta.springbatchdemo.service.PanDecryptLaunchService;
import com.zeta.springbatchdemo.service.UcicJobLaunchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class JobLaunchController {

    @Autowired
    JobLaunchService jobLaunchService;

    @Autowired
    UcicJobLaunchService ucicJobLaunchService;

    @Autowired
    PanDecryptLaunchService panDecryptLaunchService;

    @GetMapping
    public void launchEmployeeJob() throws Exception {
        System.out.println("Triggered User data job.");
        long start = System.currentTimeMillis();
        jobLaunchService.runUserDataJob();
        System.out.println("Elapsed time in millis : " + (System.currentTimeMillis() - start));
        System.out.println("Job executed");
    }

    @GetMapping("/reverse")
    public void launchReverseHandoffJob() throws Exception {
        System.out.println("Triggered reverse data job.");
        long start = System.currentTimeMillis();
        ucicJobLaunchService.runUcicJob();
        System.out.println("Elapsed time in millis : " + (System.currentTimeMillis() - start));
        System.out.println("reverse Job executed");
    }

    @GetMapping("/pan-decrypt")
    public void launchPanDecryptJob() throws Exception {
        System.out.println("Triggered launchPanDecryptJob ");
        long start = System.currentTimeMillis();
        panDecryptLaunchService.runPanDecryptJob();
        System.out.println("Elapsed time in millis : " + (System.currentTimeMillis() - start));
        System.out.println("launchPanDecrypt Job executed");
    }



//    @Autowired
//    @Qualifier("jobToLaunch")
//    private Job job;
//
//    @Autowired
//    @Qualifier("employeeJob")
//    private Job employeeJob;

//    @PostMapping
//    public void launchSampleJob(@RequestParam("name") String name) throws Exception{
//        JobParameters jobParameters = new JobParametersBuilder()
//                .addString("name", name)
//                .toJobParameters();
//        this.jobLauncher.run(job, jobParameters);
//    }


}
