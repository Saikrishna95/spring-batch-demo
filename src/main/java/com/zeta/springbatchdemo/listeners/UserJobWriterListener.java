package com.zeta.springbatchdemo.listeners;

import com.zeta.springbatchdemo.model.User;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@JobScope
public class UserJobWriterListener implements ItemWriteListener<User> {

    @Value("#{jobExecution.executionContext}")
    private ExecutionContext executionContext;

    @Override
    public void beforeWrite(List<? extends User> list) {
        System.out.println("UserJobWriterListener beforeWrite Before update: " + executionContext.getInt("fileCounter"));
        if (executionContext.containsKey("fileCounter")) {
            System.out.println("updating filecounter in listener");
            int filecount = executionContext.getInt("fileCounter");
            executionContext.putInt("fileCounter", filecount + 1);
        }
        System.out.println("UserJobWriterListener beforeWrite After Update: " + executionContext.getInt("fileCounter"));
    }

    @Override
    public void afterWrite(List<? extends User> list) {
        System.out.println("UserJobWriterListener after write");
    }

    @Override
    public void onWriteError(Exception e, List<? extends User> list) {

    }
}
