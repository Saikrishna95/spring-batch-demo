package com.zeta.springbatchdemo.listeners;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;

import java.io.File;
import java.io.IOException;
import java.util.stream.IntStream;

import static com.zeta.springbatchdemo.service.JobLaunchService.getTodayDate;

public class UserJobStepListener implements StepExecutionListener {

    @Override
    public void beforeStep(StepExecution stepExecution) {
        System.out.println("UserJobStepListener beforeStep method");
        System.out.println("before step : fileCount from step : " + stepExecution.getJobExecution().getExecutionContext().getInt("fileCounter"));
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        System.out.println("UserJobStepListener afterStep method");
        int fileCounter = stepExecution.getJobExecution().getExecutionContext().getInt("fileCounter");
        System.out.println("After step : fileCount from step : " + fileCounter);
        IntStream.range(1, fileCounter+1).forEach(fileIndex -> {
            File file = new File("/tmp/CONF_Complete_" + getTodayDate() + "_" + fileIndex + ".txt");
            try {
                file.createNewFile();
            } catch (IOException e) {
                System.out.println("Error while creating file " + e);
            }
        });
        return ExitStatus.FAILED; //NOTE: for this to work, update the Job BatchStatus to failed
    }
}
