package com.zeta.springbatchdemo.listeners;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class UserJobExecutionListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        jobExecution.getExecutionContext().putInt("fileCounter", 0);
        jobExecution.getExecutionContext().putString("fileHeaders", "user_id|username|first_name|last_name|gender|password|status");
        System.out.println("UserJobExecutionListener before job method");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
//        jobExecution.getExecutionContext().remove("fileCounter");
        System.out.println("UserJobExecutionListener after job method");
        System.out.println("jobExecution.getStatus() : " + jobExecution.getStatus());
        System.out.println("jobExecution.getExitStatus() : " + jobExecution.getExitStatus());
        if (jobExecution.getExitStatus().getExitCode() == ExitStatus.FAILED.getExitCode()) {
            jobExecution.setStatus(BatchStatus.FAILED);
        } else {
            jobExecution.setStatus(BatchStatus.COMPLETED);
        }
        System.out.println("Updated : jobExecution.getStatus() : " + jobExecution.getStatus());
    }
}
