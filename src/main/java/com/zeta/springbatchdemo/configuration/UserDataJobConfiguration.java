package com.zeta.springbatchdemo.configuration;

import com.zeta.springbatchdemo.lineAggregator.UserLineAggregator;
import com.zeta.springbatchdemo.mapper.ReverseHandoffEntryRowMapper;
import com.zeta.springbatchdemo.mapper.UserRowMapper;
import com.zeta.springbatchdemo.model.ReverseHandOffEntry;
import com.zeta.springbatchdemo.model.User;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.PostgresPagingQueryProvider;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;
import java.io.File;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

@Configuration
public class UserDataJobConfiguration {

    @Autowired
    private DataSource dataSource;

    @Bean("userDataPagingItemReader")
    public JdbcPagingItemReader<User> userDataPagingItemReader() {
        System.out.println("User data job item reader bean initiated");
        JdbcPagingItemReader<User> reader = new JdbcPagingItemReader<>();

        reader.setDataSource(dataSource);
        reader.setFetchSize(200); // Pagination size. Total db hits = total records from db / page-size
        reader.setRowMapper(new UserRowMapper());

        //NOTE not to be used in prod. Just for testing.
        reader.setMaxItemCount(104);
//        reader.setCurrentItemCount(100);// Index for the current item. Used on restarts to indicate where to start from

        PostgresPagingQueryProvider queryProvider = new PostgresPagingQueryProvider();
        queryProvider.setSelectClause("user_id, username, first_name, last_name, gender, password, status");
        queryProvider.setFromClause("from user_details");
//        queryProvider.setWhereClause("where user_id <= 10");

        Map<String, Order> sortKeys = new HashMap<>(1);
        sortKeys.put("user_id", Order.ASCENDING);
        queryProvider.setSortKeys(sortKeys);

        reader.setQueryProvider(queryProvider);
        return reader;
    }

    @Bean("reverseHandOffPagingItemReader")
    public JdbcPagingItemReader<ReverseHandOffEntry> reverseHandOffPagingItemReader() {
        System.out.println("Reverse handoff job item reader bean initiated");
        JdbcPagingItemReader<ReverseHandOffEntry> reader = new JdbcPagingItemReader<>();

        reader.setDataSource(dataSource);
        reader.setFetchSize(3); // TODO Pagination size, move to config
        reader.setRowMapper(new ReverseHandoffEntryRowMapper());

        //NOTE not to be used in prod. Just for testing.
//        reader.setMaxItemCount(100004); // TODO will be removed
//        reader.setCurrentItemCount(100);// Index for the current item. Used on restarts to indicate where to start from

        PostgresPagingQueryProvider queryProvider = new PostgresPagingQueryProvider();
        //TODO update this
        queryProvider.setSelectClause("unique_id, received_ucic, uploaded_ucic, status, reason_of_rejection");
        queryProvider.setFromClause("from user_ucic_details");
//        queryProvider.setWhereClause("where user_id <= 10");

        Map<String, Order> sortKeys = new HashMap<>(1);
        sortKeys.put("unique_id", Order.ASCENDING); //TODO
        queryProvider.setSortKeys(sortKeys);

        reader.setQueryProvider(queryProvider);
        return reader;
    }
//    @Bean
//    public ThreadPoolTaskExecutor userTaskExecutor() {
//        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
//        executor.setCorePoolSize(5);
//        executor.setMaxPoolSize(5);
//        executor.setQueueCapacity(500);
//        executor.setThreadNamePrefix("UserDataThread-");
//        executor.initialize();
//        return executor;
//    }

}
