package com.zeta.springbatchdemo.shedlock;

import com.zeta.springbatchdemo.service.UcicJobLaunchService;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.LocalTime;

/*
* How to run multiple instances to simulate concurrent hits to shedlock table for lock testing
*  mvn spring-boot:run -Dspring-boot.run.arguments="--PORT=8081"
*  mvn spring-boot:run -Dspring-boot.run.arguments="--PORT=8082"
*  mvn spring-boot:run -Dspring-boot.run.arguments="--PORT=8083"
*
* Only one instance will get the lock at given hit from above 3 instances
* */
@Component
public class ScheduledTasks {

    //https://medium.com/@marcoscarneirolima/scheduler-lock-using-spring-shedlock-b9ac3e51a934
    @Autowired
    UcicJobLaunchService ucicJobLaunchService;

    @DateTimeFormat(pattern = "HH:mm:ss")
    private LocalTime beginTime;

//    @Scheduled(cron = "0 */2 * * * ?")  // Runs every minute
//    @SchedulerLock(name = "scheduledTaskName", lockAtMostFor = "2m", lockAtLeastFor = "1m")
    public void scheduledTask() throws Exception {
        System.out.println("Running scheduled task..." + LocalDateTime.now());
        // Task implementation here
        System.out.println("Sleeping for 20sec");
        Thread.sleep(20000);

        LocalDateTime refDateTime = LocalDateTime.now();//in UTC
        LocalTime refTime = refDateTime.toLocalTime();
        beginTime  = LocalTime.parse("13:30:00");

        System.out.println("beginTime : " + beginTime);
        System.out.println("refTime : " + refTime);
        System.out.println("refDateTime : " + refDateTime);

        System.out.println("Triggered reverse data job.");
        long start = System.currentTimeMillis();
        ucicJobLaunchService.runUcicJob();
        System.out.println("Elapsed time in millis : " + (System.currentTimeMillis() - start));
        System.out.println("reverse Job executed - " + LocalDateTime.now());
    }
}
