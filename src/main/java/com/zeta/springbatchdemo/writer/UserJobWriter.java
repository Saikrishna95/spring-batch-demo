package com.zeta.springbatchdemo.writer;

import com.zeta.springbatchdemo.lineAggregator.UserLineAggregator;
import com.zeta.springbatchdemo.model.User;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;


import java.util.List;


import static com.zeta.springbatchdemo.service.JobLaunchService.getTodayDateTime;

@Component
@JobScope
public class UserJobWriter implements ItemWriter<User> {

    private int fileCounter;
    private String fileHeaders;

    @Value("#{jobExecution.executionContext}")
    private ExecutionContext executionContext;

    @Override
    public void write(List<? extends User> items) throws Exception {
        this.fileCounter = executionContext.getInt("fileCounter");
        this.fileHeaders = executionContext.getString("fileHeaders");
        System.out.println("New Writer : fileCount : " + fileCounter + ", fileHeaders : " + fileHeaders);
        System.out.println("Writer started writing items : " + items.size());
        final FlatFileItemWriter<User> itemWriter = new FlatFileItemWriter<>();
        System.out.println("currentFileNumber : " + fileCounter);
        itemWriter.setHeaderCallback(writer -> {
            String headers = "HDR| "+ items.size() + "|" + fileCounter +"\n" + fileHeaders;
            writer.write(headers);
        });

        itemWriter.setLineAggregator(new UserLineAggregator());
//        String threadName = Thread.currentThread().getName();
        itemWriter.setResource(new FileSystemResource("/tmp/USER_DATA_" + getTodayDateTime() + "_" + fileCounter + ".txt"));
        itemWriter.open(new ExecutionContext());
        itemWriter.write(items);
        itemWriter.close();
    }
}
