package com.zeta.springbatchdemo.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ReverseHandOffEntry {
    private String sourceSystem;
    private String uniqueId;
    private String receivedUCIC;
    private String uploadedUCIC;
    private String status;
    private String reasonOfRejection;

    public ReverseHandOffEntry(String sourceSystem, String uniqueId, String receivedUCIC,
                               String uploadedUCIC, String status, String reasonOfRejection) {
        this.sourceSystem = sourceSystem;
        this.uniqueId = uniqueId;
        this.receivedUCIC = receivedUCIC;
        this.uploadedUCIC = uploadedUCIC;
        this.status = status;
        this.reasonOfRejection = reasonOfRejection;
    }
}
