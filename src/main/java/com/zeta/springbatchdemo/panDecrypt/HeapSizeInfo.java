package com.zeta.springbatchdemo.panDecrypt;

public class HeapSizeInfo {
    public static void main(String[] args) {
        // Get the Java runtime
        Runtime runtime = Runtime.getRuntime();

        // Print the default maximum heap size
        long maxMemory = runtime.maxMemory();
        System.out.println("Default Maximum Heap Size: " + (maxMemory / (1024 * 1024)) + " MB");

        // Print the default initial heap size (not directly available, but can infer from usage)
        long totalMemory = runtime.totalMemory();
        System.out.println("Default Initial Heap Size: " + (totalMemory / (1024 * 1024)) + " MB");

        // Print the default free heap size
        long freeMemory = runtime.freeMemory();
        System.out.println("Free Heap Size: " + (freeMemory / (1024 * 1024)) + " MB");
    }
}