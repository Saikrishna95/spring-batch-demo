package com.zeta.springbatchdemo.panDecrypt;

import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
@JobScope
public class PanDecryptStepWriterListener implements ItemWriteListener<PanDecryptOutput> {


    @Value("#{jobExecution.executionContext}")
    private ExecutionContext executionContext;

    @Override
    public void beforeWrite(List<? extends PanDecryptOutput> list) {
        executionContext.putInt("fileCounter", executionContext.getInt("fileCounter") + 1);
        System.out.println("Updated fileCounter to : " + executionContext.getInt("fileCounter"));
    }

    @Override
    public void afterWrite(List<? extends PanDecryptOutput> list){
        System.out.println("Executing SourceSystemStepWriterListener afterwrite() method");
    }

    @Override
    public void onWriteError(Exception e, List<? extends PanDecryptOutput> list) {
        System.out.println("Executing SourceSystemStepWriterListener onWriteError() method");
    }
}