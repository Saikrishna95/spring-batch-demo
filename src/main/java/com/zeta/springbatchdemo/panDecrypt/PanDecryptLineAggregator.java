package com.zeta.springbatchdemo.panDecrypt;

import org.springframework.batch.item.file.transform.LineAggregator;

import java.util.StringJoiner;

public class PanDecryptLineAggregator implements LineAggregator<PanDecryptOutput> {

    private static String COMMA_DELIMITER = ",";
    @Override
    public String aggregate(PanDecryptOutput panDecryptOutput) {
        StringJoiner data = new StringJoiner(COMMA_DELIMITER);

        data.add(panDecryptOutput.getCustomerId());
        data.add(panDecryptOutput.getPanEncrypted());
        data.add(panDecryptOutput.getPanDecrypted());

        return data.toString();
    }
}
