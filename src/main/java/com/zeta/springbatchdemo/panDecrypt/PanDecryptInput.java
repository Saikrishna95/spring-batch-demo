package com.zeta.springbatchdemo.panDecrypt;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@NoArgsConstructor
public class PanDecryptInput {
    private String customerId= "";
    private String panEncrypted = "";
}
