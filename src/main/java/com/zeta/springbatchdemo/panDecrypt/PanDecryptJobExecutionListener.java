package com.zeta.springbatchdemo.panDecrypt;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.stereotype.Component;

@Component
@JobScope
public class PanDecryptJobExecutionListener implements JobExecutionListener {


    @Override
    public void beforeJob(JobExecution jobExecution) {

        System.out.println("Initiating fileCounter as 0 in job execution.");
        jobExecution.getExecutionContext().putInt("fileCounter", 0);
        System.out.println("Job parameters : " + jobExecution.getJobParameters());
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println("UcicJobExecutionListener : afterJob() ");
    }
}
