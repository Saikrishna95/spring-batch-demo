package com.zeta.springbatchdemo.panDecrypt;

import org.springframework.batch.core.configuration.annotation.JobScope;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;


@Configuration
public class PanDecryptConfig {

    private int fileCounter;
    public static String UNDERSCORE = "_";
    public static String FILE_EXTENSION_CSV= ".csv";
    private PayloadEncryptionDecryptionUtil payloadEncryptionDecryptionUtil;

    @Autowired
    public PanDecryptConfig(PayloadEncryptionDecryptionUtil payloadEncryptionDecryptionUtil){
        this.payloadEncryptionDecryptionUtil = payloadEncryptionDecryptionUtil;
    }

    @Bean("panDecryptItemReader")
    @JobScope
    public FlatFileItemReader<PanDecryptInput> panDecryptItemReader(){

        BeanWrapperFieldSetMapper<PanDecryptInput> beanWrapperFieldSetMapper = new BeanWrapperFieldSetMapper<>();
        beanWrapperFieldSetMapper.setTargetType(PanDecryptInput.class);

        String filePath = "/Users/sk/Downloads/Wibmo_Users_Encrypted_PAN.csv";
//        String filePath = "/Users/sk/Downloads/encryptPanData.csv";

        return new FlatFileItemReaderBuilder<PanDecryptInput>()
                .name("panDecryptItemReader")
                .resource(new FileSystemResource(filePath))
                .delimited()
                .names("customer_id", "pan_encrypted")
                .linesToSkip(1)
                .fieldSetMapper(beanWrapperFieldSetMapper)
                .build();
    }

    @Bean("panDecryptItemProcessor")
    public ItemProcessor<PanDecryptInput, PanDecryptOutput> panDecryptItemProcessor() {
        return panDecryptInput -> {
            String cipherText = panDecryptInput.getPanEncrypted();
            System.out.println("cipherText : " + cipherText);
            String plaintext = payloadEncryptionDecryptionUtil.decryptPanAndDob(cipherText);
            System.out.println("Plaintext : " + plaintext);

            PanDecryptOutput panDecryptOutput = PanDecryptOutput.builder()
                    .customerId(panDecryptInput.getCustomerId())
                    .panEncrypted(panDecryptInput.getPanEncrypted())
                    .panDecrypted(plaintext)
                    .build();

            return panDecryptOutput;
        };
    }

    @Bean("panDecryptItemWriter")
    @JobScope
    public ItemWriter<PanDecryptOutput> panDecryptItemWriter(
            @Value("#{jobExecution.executionContext}") ExecutionContext executionContext) {
        return items -> {
            // NOTE for every chunk when writer is used, we are creating a new file.
            System.out.println("panDecryptItemWriter : " + items.size());
            final FlatFileItemWriter<PanDecryptOutput> itemWriter = new FlatFileItemWriter<>();
            this.fileCounter = executionContext.getInt("fileCounter");
            itemWriter.setLineAggregator(new PanDecryptLineAggregator());

            String finalOutputPath = "/Users/sk/Downloads/Wibmo_Users_Decrypted_PAN_data"
                    + UNDERSCORE + fileCounter + FILE_EXTENSION_CSV;

            itemWriter.setResource(new FileSystemResource(finalOutputPath));
            itemWriter.open(new ExecutionContext());
            itemWriter.write(items);
            itemWriter.close();
        };
    }



}
