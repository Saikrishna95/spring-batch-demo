package com.zeta.springbatchdemo.panDecrypt;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.bouncycastle.crypto.params.KeyParameter;
import org.springframework.stereotype.Component;

@Component
public class PayloadEncryptionDecryptionUtil {
    public static final String PAYLOAD_ENCRYPT_DECRYPT_ALGORITHM = "AES/GCM/NoPadding";
    public static final int TAG_LENGTH_BIT = 128;
    public static final int IV_LENGTH_BYTE = 12;
    public static final int SALT_LENGTH_BYTE = 16;
    public static final int ITERATION_COUNT = 10;
    public static final int KEY_LENGTH = 256;
    private static final String SECRETKEY = "rag9EPYvwnOLWb04206pHD83xSq+vyZ1LSwgJZJWDZ4=";
    private static final byte[] NONCE = new byte[]{92, -22, -2, 83, -122, -84, 105, -105, -40, -44, 32, -37};

//    public static void main(String[] args) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
//        String cipherText = "7FyTZ5xcLTmv2W+jgycEsGG/uHuDPvycJMI=";
//        System.out.println("cipherText : " + cipherText);
//        PayloadEncryptionDecryptionUtil payloadEncryptionDecryptionUtil = new PayloadEncryptionDecryptionUtil();
//        String plaintext = payloadEncryptionDecryptionUtil.decryptPanAndDob(cipherText);
//        System.out.println("Plaintext : " + plaintext);
//    }

    public PayloadEncryptionDecryptionUtil() {
    }

    public String encryptPanAndDob(String plainText) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return this.encryptPanAndDob(plainText, "rag9EPYvwnOLWb04206pHD83xSq+vyZ1LSwgJZJWDZ4=");
    }

    public String decryptPanAndDob(String cipherText) throws InvalidAlgorithmParameterException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        return this.decryptPanAndDob(cipherText, "rag9EPYvwnOLWb04206pHD83xSq+vyZ1LSwgJZJWDZ4=");
    }

    public String encryptPayload(String plainText, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] salt = this.getRandomNonce(16);
        byte[] iv = this.getRandomNonce(12);
        SecretKey aesKeyFromPassword = this.getAESKeyFromSecretKey(Base64.getDecoder().decode(secretKey.getBytes()), salt);
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(1, aesKeyFromPassword, new GCMParameterSpec(128, iv));
        byte[] cipherText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        byte[] cipherTextWithIvSalt = ByteBuffer.allocate(salt.length + iv.length + cipherText.length).put(salt).put(iv).put(cipherText).array();
        return Base64.getEncoder().encodeToString(cipherTextWithIvSalt);
    }

    public String decryptPayload(String cipherText, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        byte[] decode = Base64.getDecoder().decode(cipherText.getBytes(StandardCharsets.UTF_8));
        ByteBuffer bb = ByteBuffer.wrap(decode);
        byte[] salt = new byte[16];
        bb.get(salt);
        byte[] iv = new byte[12];
        bb.get(iv);
        byte[] cText = new byte[bb.remaining()];
        bb.get(cText);
        SecretKey aesKeyFromPassword = this.getAESKeyFromSecretKey(Base64.getDecoder().decode(secretKey.getBytes()), salt);
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(2, aesKeyFromPassword, new GCMParameterSpec(128, iv));
        byte[] plainText = cipher.doFinal(cText);
        return new String(plainText, StandardCharsets.UTF_8);
    }

    public byte[] getRandomNonce(int noOfBytes) {
        if (noOfBytes <= 0) {
            noOfBytes = 16;
        }

        byte[] nonce = new byte[noOfBytes];
        (new SecureRandom()).nextBytes(nonce);
        return nonce;
    }

    public SecretKey getAESKeyFromSecretKey(byte[] password, byte[] salt) {
        PKCS5S2ParametersGenerator gen = new PKCS5S2ParametersGenerator(new SHA256Digest());
        gen.init(password, salt, 10);
        byte[] dk = ((KeyParameter)gen.generateDerivedParameters(256)).getKey();
        return new SecretKeySpec(dk, "AES");
    }

    public String encryptPanAndDob(String plainText, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(1, this.stringToSecretKey(secretKey), new GCMParameterSpec(128, NONCE));
        byte[] cipherText = cipher.doFinal(plainText.getBytes(StandardCharsets.UTF_8));
        return Base64.getEncoder().encodeToString(cipherText);
    }

    public String decryptPanAndDob(String cipherText, String secretKey) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        cipher.init(2, this.stringToSecretKey(secretKey), new GCMParameterSpec(128, NONCE));
        byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText.getBytes(StandardCharsets.UTF_8)));
        return new String(plainText, StandardCharsets.UTF_8);
    }

    public SecretKey stringToSecretKey(String secretKey) {
        byte[] decodedKey = Base64.getDecoder().decode(secretKey);
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }
}
