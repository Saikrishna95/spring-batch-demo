package com.zeta.springbatchdemo.panDecrypt;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class PanDecryptOutput {
    private String customerId= "";
    private String panEncrypted = "";
    private String panDecrypted = "";
}
