package com.zeta.springbatchdemo.service;

import com.zeta.springbatchdemo.lineAggregator.ReverseHandoffLineAggregator;
import com.zeta.springbatchdemo.model.ReverseHandOffEntry;
import com.zeta.springbatchdemo.panDecrypt.PanDecryptInput;
import com.zeta.springbatchdemo.panDecrypt.PanDecryptOutput;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class UcicJobLaunchService {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    private String jobIdPrefix = "ReverseJobTest_";

    private AtomicInteger fileCounter;

    private static final String REVERSE_HANDOFF_HEADERS = "SOURCE_SYSTEM|UNQ_ID|RECIEVED_UCIC|UPLOADED_UCIC|STATUS|REASON FOR REJECTION";

    // NOTE creating this as method in this class didn't work.
    // Enabled it as Bean and autowired, then it worked.
    @Autowired
    JdbcPagingItemReader<ReverseHandOffEntry> reverseHandOffPagingItemReader;

    public void runUcicJob() throws Exception {
        try {
            printHeapSizeDetails();
            this.jobLauncher.run(getUcicJob(), new JobParametersBuilder()
                    .addString("date", getTodayDate())
                    .addString("currentMinute", String.valueOf(getCurrentMinute()))
                    .toJobParameters());
        } catch (Exception exception) {
            log.error("Exception : ", exception);
        }
    }

    public String jobId() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHH:mm:ss");
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDateTime localDateTime = LocalDateTime.now();
        String dateString = dateTimeFormatter.format(localDateTime);
        System.out.println(dateString);
        return jobIdPrefix + dateString + "_" + getCurrentMinute();
    }

    public Job getUcicJob() throws Exception {
        fileCounter = new AtomicInteger(0);
        System.out.println("New Reverse handoff job started. file count : " + fileCounter.get());
//        System.out.println("Sleeping for 20 sec");
//        Thread.sleep(20000);

        return jobBuilderFactory.get(jobId())
                .start(getUcicJobStep()) // TODO pass job name here
                .build();
    }

    public Step getUcicJobStep() throws Exception {
        return stepBuilderFactory.get("step1" + LocalDateTime.now())
                .<ReverseHandOffEntry, ReverseHandOffEntry>chunk(3)
                .reader(ucicDataReader())
//                .processor(null) // encrypt in processor
                .writer(ucicDataWriter()) // write to file here
                .build();
    }

    public ItemReader<ReverseHandOffEntry> ucicDataReader() {
        return reverseHandOffPagingItemReader; // file reader
    }

    public ItemWriter<ReverseHandOffEntry> ucicDataWriter() {
        System.out.println("New Reverse handoff job item writer lambda is submitted.");
        return items -> { // NOTE for every chunk when writer is used, we are creating a new file.
            System.out.println("Writer started writing items : " + items.size());
            final FlatFileItemWriter<ReverseHandOffEntry> itemWriter = new FlatFileItemWriter<>();
            int currentFileNumber = fileCounter.incrementAndGet();
            System.out.println("currentFileNumber : " + currentFileNumber);
            itemWriter.setHeaderCallback(writer -> {
                String headers = "HDR|"+ items.size() + "|" + currentFileNumber +"\n" + REVERSE_HANDOFF_HEADERS;
                writer.write(headers);
            });

            itemWriter.setLineAggregator(new ReverseHandoffLineAggregator());
            // TODO push to SFTP once we create a file
            itemWriter.setResource(new FileSystemResource("/tmp/ReverseHandoff_" + getTodayDate() + "_" + currentFileNumber + ".txt"));
            itemWriter.open(new ExecutionContext());
            itemWriter.write(items);
            itemWriter.close();
        };
    }
    private String getTodayDate() {
        DateTimeFormatter FOMATTER = DateTimeFormatter.ofPattern("yyyyMMdd");
        LocalDate localDate = LocalDate.now();
        String dateString = FOMATTER.format(localDate);
        return dateString;
    }

    public long getCurrentMinute() {
        LocalDateTime now = LocalDateTime.now();

        // Get the time part of the current date and time
        LocalTime currentTime = now.toLocalTime();

        // Define midnight time
        LocalTime midnight = LocalTime.of(0, 0);

        // Calculate the duration between midnight and the current time
        Duration duration = Duration.between(midnight, currentTime);

        // Get the number of minutes
        long minutesPassed = duration.toMinutes();

        System.out.println("Minutes passed since midnight: " + minutesPassed);
        return minutesPassed;
    }

    private void printHeapSizeDetails() {
        Runtime runtime = Runtime.getRuntime();

        // Print the default maximum heap size
        long maxMemory = runtime.maxMemory();
        System.out.println("Default Maximum Heap Size: " + (maxMemory / (1024 * 1024)) + " MB");

        // Print the default initial heap size (not directly available, but can infer from usage)
        long totalMemory = runtime.totalMemory();
        System.out.println("Default Initial Heap Size: " + (totalMemory / (1024 * 1024)) + " MB");

        // Print the default free heap size
        long freeMemory = runtime.freeMemory();
        System.out.println("Free Heap Size: " + (freeMemory / (1024 * 1024)) + " MB");
    }
}
