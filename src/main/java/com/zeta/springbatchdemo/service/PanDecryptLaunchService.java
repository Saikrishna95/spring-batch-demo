package com.zeta.springbatchdemo.service;

import com.zeta.springbatchdemo.panDecrypt.PanDecryptInput;
import com.zeta.springbatchdemo.panDecrypt.PanDecryptJobExecutionListener;
import com.zeta.springbatchdemo.panDecrypt.PanDecryptOutput;
import com.zeta.springbatchdemo.panDecrypt.PanDecryptStepWriterListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@Slf4j
public class PanDecryptLaunchService {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;


    @Autowired
    FlatFileItemReader<PanDecryptInput> panDecryptItemReader;

    @Autowired
    ItemProcessor<PanDecryptInput, PanDecryptOutput> panDecryptItemProcessor;

    @Autowired
    ItemWriter<PanDecryptOutput> panDecryptItemWriter;

    @Autowired
    PanDecryptJobExecutionListener panDecryptJobExecutionListener;

    @Autowired
    PanDecryptStepWriterListener panDecryptStepWriterListener;

    private String jobIdPrefix = "PanDecrypt_";
    private int CHUNK_SIZE = 1000000;
    public static final String FILE_COUNTER = "fileCounter";

    public void runPanDecryptJob() throws Exception {
        try {
            printHeapSizeDetails();
            this.jobLauncher.run(getPanDecryptJob(), new JobParametersBuilder()
                    .addString("jobname", "runPanDecryptJob")
                    .toJobParameters());
        } catch (Exception exception) {
            log.error("Exception : ", exception);
        }
    }

    public Job getPanDecryptJob() throws Exception {
        System.out.println("getPanDecryptJob started");

        return jobBuilderFactory.get(jobId())
                .start(getPanDecryptJobStep())
                .listener(panDecryptJobExecutionListener)
                .build();
    }

    public String jobId() {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMddHH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();
        String dateString = dateTimeFormatter.format(localDateTime);
        System.out.println(dateString);
        return jobIdPrefix + dateString;
    }

    public Step getPanDecryptJobStep() throws Exception {
        return stepBuilderFactory.get("PanDecryptJobStep" + LocalDateTime.now())
                .<PanDecryptInput, PanDecryptOutput>chunk(CHUNK_SIZE)
                .reader(panDecryptItemReader)
                .processor(panDecryptItemProcessor) // encrypt in processor
                .writer(panDecryptItemWriter)
                .listener(panDecryptStepWriterListener)
                .build();
    }

    private void printHeapSizeDetails() {
        Runtime runtime = Runtime.getRuntime();

        // Print the default maximum heap size
        long maxMemory = runtime.maxMemory();
        System.out.println("Default Maximum Heap Size: " + (maxMemory / (1024 * 1024)) + " MB");

        // Print the default initial heap size (not directly available, but can infer from usage)
        long totalMemory = runtime.totalMemory();
        System.out.println("Default Initial Heap Size: " + (totalMemory / (1024 * 1024)) + " MB");

        // Print the default free heap size
        long freeMemory = runtime.freeMemory();
        System.out.println("Free Heap Size: " + (freeMemory / (1024 * 1024)) + " MB");
    }
}
