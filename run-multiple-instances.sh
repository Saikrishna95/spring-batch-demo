#!/bin/bash

# Run the first instance on port 8081
mvn spring-boot:run -Dspring-boot.run.arguments="--PORT=8081" &
# Run the second instance on port 8082
mvn spring-boot:run -Dspring-boot.run.arguments="--PORT=8082" &
